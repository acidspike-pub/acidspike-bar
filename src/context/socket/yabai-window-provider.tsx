import React, { createContext, ReactNode, useEffect, useState } from 'react'
import {
  initSockets,
  registerCommand,
  runCommand,
  unregisterCommand
} from './socket-igniter'

export interface YabaiSpaceWindowsData {
  'is-visible': boolean
  app: string
  id: number
  pid: number
}

export interface YabaiSpaceWindowsContextData {
  windows: Array<YabaiSpaceWindowsData>
  index?: number
  switch?: Function
}

export const YabaiSpaceWindowsContext =
  createContext<YabaiSpaceWindowsContextData>({
    windows: []
  })

interface Props {
  children: ReactNode
  spaceIndex: number
}

export const YabaiSpaceWindowsProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<YabaiSpaceWindowsContextData>({
    windows: []
  })

  useEffect(() => {
    initYabaiSpaceWindowsSockets(setValue, props.spaceIndex)

    return () => {
      unregisterCommand(`getYabaiSpace${props.spaceIndex}Windows`)
    }
  }, [props.spaceIndex])

  return (
    <YabaiSpaceWindowsContext.Provider value={value}>
      {props.children}
    </YabaiSpaceWindowsContext.Provider>
  )
}

export const initYabaiSpaceWindowsSockets = (
  setValue: React.Dispatch<React.SetStateAction<YabaiSpaceWindowsContextData>>,
  spaceNumber: number
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerCommand(
    `getYabaiSpace${spaceNumber}Windows`,
    '/usr/local/bin/yabai',
    ['-m', 'query', '--windows', '--space', '' + spaceNumber],
    1000,
    data => {
      try {
        let jsonSpaces = JSON.parse(data)
        jsonSpaces = jsonSpaces.filter((w: any) => {
          return (
            !w['is-hidden'] && process.pid !== w.pid && process.ppid !== w.pid
          )
        })
        setValue({
          windows: jsonSpaces,
          index: spaceNumber,
          switch: () => {
            runCommand('/usr/local/bin/yabai', [
              '-m',
              'space',
              '--focus',
              '' + spaceNumber
            ])
          }
        })
      } catch (e) {
        console.error('Failed to parse JSON data', e)
      }
    }
  )
}
