import * as si from 'systeminformation'

let interval: NodeJS.Timer = null
let running = false

export function handleNetworkData(server) {
  if (interval) {
    return
  }

  interval = setInterval(() => {
    if (running) {
      return
    }

    running = true
    si.networkInterfaceDefault()
      .then(interfaceName => {
        return si.networkInterfaces().then(interfaces => {
          return { interfaces, interfaceName }
        })
      })
      .then(({ interfaces, interfaceName }) => {
        const currentInterface = interfaces.find(
          netInterface => netInterface.iface === interfaceName
        )

        return currentInterface
      })
      .then(currentInterface => {
        if (currentInterface.type === 'wireless') {
          return si.wifiConnections().then(connections => {
            return {
              currentInterface,
              connection: connections[0]
            }
          })
        }

        return { currentInterface }
      })
      .then(networkData => {
        return si
          .networkStats(networkData.currentInterface.iface)
          .then(stats => {
            return {
              ...networkData,
              stats: stats[0]
            }
          })
      })
      .then(networkData => {
        server.emit('network-data', networkData)
        running = false
      })
      .catch(() => {
        running = false
      })
  }, 1000)
}
