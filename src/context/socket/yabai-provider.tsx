import React, { createContext, ReactNode, useEffect, useState } from 'react'
import {
  initSockets,
  registerCommand,
  unregisterCommand
} from './socket-igniter'

export interface YabaiSpaceData {
  id: number
  uuid: string
  index: number
  'has-focus': boolean
  display: number
}

export interface YabaiSpacesContextData {
  data: Array<YabaiSpaceData>
}

export const YabaiSpacesContext = createContext<YabaiSpacesContextData>({
  data: []
})

interface Props {
  children: ReactNode
}

export const YabaiSpacesProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<YabaiSpacesContextData>({
    data: []
  })

  useEffect(() => {
    initYabaiSpacesSockets(setValue)

    return () => {
      unregisterCommand('getYabaiSpaces')
    }
  }, [])

  return (
    <YabaiSpacesContext.Provider value={value}>
      {props.children}
    </YabaiSpacesContext.Provider>
  )
}

export const initYabaiSpacesSockets = (
  setValue: React.Dispatch<React.SetStateAction<YabaiSpacesContextData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerCommand(
    'getYabaiSpaces',
    '/usr/local/bin/yabai',
    ['-m', 'query', '--spaces'],
    1000,
    data => {
      try {
        const jsonSpaces = JSON.parse(data)
        setValue({
          data: jsonSpaces.filter(
            (s: any) => s.display === (window as any).displayIndex + 1
          )
        })
      } catch (e) {
        console.error('Failed to parse JSON data', e)
      }
    }
  )
}
