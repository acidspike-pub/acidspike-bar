import { Server } from 'socket.io'
import {
  handleRegisterCommand,
  handleUnregisterCommand
} from './handlers/command-handler'
import { handleNetworkData } from './handlers/network-data-handler'
import { handleCpuData } from './handlers/cpu-data-handler'
import { handleMemoryData } from './handlers/memory-data-handler'
import { handleBatteryData } from './handlers/battery-data-handler'
import { handleKeyboardData } from './handlers/keyboard-data-handler'
import { handleRunCommand } from './handlers/run-command-handler'
import execa from 'execa'

let server: Server = null

function handleEvents(socket) {
  socket.on('registerCommand', data => {
    handleRegisterCommand(data, server)
  })

  socket.on('unregisterCommand', data => {
    handleUnregisterCommand(data)
  })

  socket.on('runCommand', data => {
    handleRunCommand(data)
  })

  socket.on('registerListeners', ({ internalCommandName }) => {
    switch (internalCommandName) {
      case 'network-data':
        handleNetworkData(server)
        break
      case 'cpu-data':
        handleCpuData(server)
        break
      case 'memory-data':
        handleMemoryData(server)
        break
      case 'battery-data':
        handleBatteryData(server)
        break
      case 'keyboard-data':
        handleKeyboardData(server)
        break
      default:
        console.warn('Unknown internal command name:', internalCommandName)
        break
    }
  })
}

export const StartSocketServer = () => {
  if (server) {
    return
  }

  execa('/usr/local/bin/yabai', [
    '-m',
    'rule',
    '--add',
    'title="^Acidspike Bar$"',
    'manage=off',
    'opacity=1'
  ])
    .then(out => {
      console.log('Added yabai rule', out.stdout, out.stderr)
    })
    .catch(err => {
      console.error('Failed to add yabai rule', err)
    })

  server = new Server({
    cors: (req, callback) => {
      callback(null)
    }
  })

  server.on('connection', socket => {
    console.log('Socket connected on server')

    handleEvents(socket)
  })

  server.listen(16802)
}
