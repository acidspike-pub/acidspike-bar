import execa from 'execa'

export function handleRunCommand(data) {
  console.log('Run command:', data.command)
  execa(data.command, data.args)
    .then(output => {})
    .catch(err => {
      console.error(err)
    })
}
