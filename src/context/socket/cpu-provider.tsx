import React, { ReactNode, useEffect, useState } from 'react'
import { initSockets, registerListeners } from './socket-igniter'

export interface CpuData {
  history: Array<number>
}

export const CpuDataContext = React.createContext<CpuData>({ history: [] })

interface Props {
  children: ReactNode
}

export const CpuDataProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<CpuData>({ history: [] })

  useEffect(() => {
    initCpuDataSockets(setValue)
  }, [])

  return (
    <CpuDataContext.Provider value={value}>
      {props.children}
    </CpuDataContext.Provider>
  )
}

const history: Array<number> = []

export const initCpuDataSockets = (
  setValue: React.Dispatch<React.SetStateAction<CpuData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerListeners('cpu-data', data => {
    history.push(data.currentLoad)
    let numberOfSample = 15
    if (history.length > numberOfSample) {
      const delta = history.length - numberOfSample
      history.splice(0, delta)
    }
    setValue({
      history
    })
  })
}
