import * as si from 'systeminformation'

let interval: NodeJS.Timer = null
let running = false

export function handleBatteryData(server) {
  if (interval) {
    return
  }

  interval = setInterval(() => {
    if (running) {
      return
    }

    running = true
    si.battery()
      .then(battery => {
        server.emit('battery-data', battery)
        running = false
      })
      .catch(() => {
        running = false
      })
  }, 1000)
}
