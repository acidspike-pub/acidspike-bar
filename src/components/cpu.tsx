import React, { useContext } from 'react'
import { CpuData, CpuDataContext } from '../context/socket/cpu-provider'
import { Sparklines, SparklinesBars } from 'react-sparklines-typescript'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'

export const Cpu: React.FC = () => {
  const cpuContext = useContext<CpuData>(CpuDataContext)

  return (
    <div
      className={
        'flex mr-1 bg-blue-400/50 text-white rounded-sm py-0.25 px-1 text-sm items-center w-[150px] h-full'
      }
    >
      <FontAwesomeIcon icon={solid('microchip')} className={'mr-1'} />
      <Sparklines
        data={cpuContext.history}
        style={{ flex: 1, maxHeight: 'calc(100% - 0.25rem)' }}
        min={0}
        max={100}
      >
        <SparklinesBars style={{ fill: '#ffffff' }} />
      </Sparklines>
    </div>
  )
}
