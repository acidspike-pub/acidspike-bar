import * as si from 'systeminformation'

let interval: NodeJS.Timer = null
let running = false

export function handleCpuData(server) {
  if (interval) {
    return
  }

  interval = setInterval(() => {
    if (running) {
      return
    }

    running = true
    si.currentLoad()
      .then(load => {
        server.emit('cpu-data', load)
        running = false
      })
      .catch(() => {
        running = false
      })
  }, 1000)
}
