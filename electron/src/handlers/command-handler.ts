import execa from 'execa'

const commands = new Map<
  string,
  {
    command: string
    args: Array<string>
    interval: number
    intervalInstance: NodeJS.Timer
    timeout: number
  }
>()

const running = new Set<string>()

export function handleRegisterCommand(data, server) {
  console.log('Received registerCommand:', data.commandName)
  if (commands.has(data.commandName)) {
    const dataCommand = commands.get(data.commandName)
    clearInterval(dataCommand.intervalInstance)
  }

  commands.set(data.commandName, {
    command: data.command,
    interval: data.interval,
    args: data.args,
    timeout: data.timeout || 5000,
    intervalInstance: setInterval(() => {
      if (running.has(data.commandName)) {
        return
      }

      running.add(data.commandName)

      const handler = execa(data.command, data.args)
      handler
        .then(output => {
          server.emit(data.commandName, output.stdout)
          running.delete(data.commandName)
        })
        .catch(err => {
          console.error(err)
          running.delete(data.commandName)
        })

      setTimeout(() => {
        handler.kill()
      }, data.timeout || 5000)
    }, data.interval)
  })
}

export function handleUnregisterCommand(data) {
  if (commands.has(data.commandName)) {
    console.log('Unregister command:', data.commandName)
    const dataCommand = commands.get(data.commandName)
    clearInterval(dataCommand.intervalInstance)
    commands.delete(data.commandName)
  }
}
