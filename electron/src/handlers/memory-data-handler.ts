import * as si from 'systeminformation'

let interval: NodeJS.Timer = null
let running = false

export function handleMemoryData(server) {
  if (interval) {
    return
  }

  interval = setInterval(() => {
    if (running) {
      return
    }

    running = true
    si.mem()
      .then(mem => {
        server.emit('memory-data', mem)
        running = false
      })
      .catch(() => {
        running = false
      })
  }, 1000)
}
