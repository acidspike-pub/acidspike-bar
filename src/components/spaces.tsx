import Space from './space'
import { useContext } from 'react'
import { YabaiSpacesContext } from '../context/socket/yabai-provider'
import { YabaiSpaceWindowsProvider } from '../context/socket/yabai-window-provider'

export default function Spaces() {
  const data = useContext(YabaiSpacesContext)

  return (
    <div className={'flex items-center p-1'}>
      {data.data.map(d => (
        <YabaiSpaceWindowsProvider spaceIndex={d.index} key={d.uuid}>
          <Space key={d.uuid} space={d}></Space>
        </YabaiSpaceWindowsProvider>
      ))}
    </div>
  )
}
