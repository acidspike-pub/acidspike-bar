import React, { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'
import {
  KeyboardData,
  KeyboardDataContext
} from '../context/socket/keyboard-provider'

export const Keyboard: React.FC = () => {
  const keyboardContext = useContext<KeyboardData>(KeyboardDataContext)

  return (
    <div
      className={
        'flex mr-1 bg-amber-400/50 text-white rounded-sm py-0.25 px-1 text-sm items-center h-full'
      }
    >
      <FontAwesomeIcon icon={solid('keyboard')} className={'mr-2'} />
      {keyboardContext.keyboardLayout}
    </div>
  )
}
