import React, { useContext, useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'
import {
  MemoryData,
  MemoryDataContext
} from '../context/socket/memory-provider'

export const Memory: React.FC = () => {
  const memoryContext = useContext<MemoryData>(MemoryDataContext)
  const [heightPercent, setHeightPercent] = useState<number>(0)

  useEffect(() => {
    const p = (memoryContext.active / memoryContext.total) * 100

    setHeightPercent(p)
  }, [memoryContext])

  return (
    <div
      className={
        'flex mr-1 bg-violet-400/50 text-white rounded-sm py-0.25 px-1 text-sm items-center h-full'
      }
    >
      <FontAwesomeIcon icon={solid('memory')} className={'mr-2'} />
      <div
        className={'w-2 bg-white self-end mb-0.5'}
        style={{ height: `${heightPercent}%` }}
      ></div>
    </div>
  )
}
