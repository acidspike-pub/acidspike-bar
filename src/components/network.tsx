import React, { useContext, useEffect, useState } from 'react'
import { NetworkDataContext } from '../context/socket/network-provider'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'
import { IconDefinition } from '@fortawesome/free-solid-svg-icons'

interface Props {}

export const Network: React.FC = (props: Props) => {
  const networkDataContext = useContext(NetworkDataContext)
  const [iconValue, setIconValue] = useState<IconDefinition>(solid('wifi'))

  useEffect(() => {
    if (
      networkDataContext.currentInterface?.type === 'wireless' &&
      networkDataContext.connection
    ) {
      setIconValue(solid('wifi'))
    } else if (networkDataContext.currentInterface?.type === 'wireless') {
      setIconValue(solid('globe'))
    } else {
      setIconValue(solid('ethernet'))
    }
  }, [networkDataContext])

  return (
    <div
      className={
        'flex mr-1 bg-orange-500/50 text-white rounded-sm py-0.25 px-1 text-sm items-center'
      }
    >
      <FontAwesomeIcon icon={solid('arrow-up')} />
      &nbsp;
      {networkDataContext.stats?.tx_sec_string}/s&nbsp;&nbsp;
      <FontAwesomeIcon icon={solid('arrow-down')} />
      &nbsp;
      {networkDataContext.stats?.rx_sec_string}/s&nbsp;-&nbsp;
      <FontAwesomeIcon icon={iconValue} className={'mr-1'} />
      {networkDataContext.connection?.ssid}&nbsp;
    </div>
  )
}
