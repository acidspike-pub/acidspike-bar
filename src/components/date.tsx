import { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'

export const DateCalendar: React.FC = props => {
  const [value, setValue] = useState<string>('')

  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date()
      setValue(
        date.toLocaleDateString(undefined, {
          month: 'long',
          day: '2-digit',
          weekday: 'long',
          year: 'numeric'
        })
      )
    })

    return () => clearInterval(interval)
  })
  return (
    <div
      className={
        'flex text-sm px-1 py-0.25 rounded-sm items-center text-white bg-teal-500/50 mr-1'
      }
    >
      <FontAwesomeIcon icon={solid('calendar')} className={'mr-1.5'} />
      {value}
    </div>
  )
}
