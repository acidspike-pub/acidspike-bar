import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'fr.acidspike.bar',
  appName: 'acidspike-bar',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
