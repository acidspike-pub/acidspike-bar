import { YabaiSpaceData } from '../context/socket/yabai-provider'
import { useContext, useEffect, useState } from 'react'
import { YabaiSpaceWindowsContext } from '../context/socket/yabai-window-provider'

interface Props {
  space: YabaiSpaceData
}

export default function Space(props: Props) {
  const yabaiSpaceContext = useContext(YabaiSpaceWindowsContext)
  const [apps, setApps] = useState<string>('')
  const [isHover, setIsHover] = useState(false)
  const [bgClass, setBgClass] = useState('bg-gray-400/50')
  const [textClass, setTextClass] = useState('text-white')

  useEffect(() => {
    const appNames =
      ' ' +
      yabaiSpaceContext.windows
        .map(w => {
          return w.app
        })
        .join(' | ')

    setApps(appNames)
    if (props.space['has-focus']) {
      setBgClass('bg-gray-400')
      setTextClass('text-gray-700')
    } else if (yabaiSpaceContext.windows.length > 0) {
      setBgClass('bg-gray-500/50')
      setTextClass('text-white')
    } else {
      setBgClass('bg-gray-700/50')
      setTextClass('text-white')
    }
  }, [yabaiSpaceContext, props.space])

  function changeSpace() {
    if (yabaiSpaceContext.switch) {
      yabaiSpaceContext.switch()
    }
  }

  return (
    <div
      className={`flex rounded-sm ${bgClass} px-1 py-0.25 ${textClass} text-sm mr-1 cursor-pointer`}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onClick={() => changeSpace()}
    >
      {props.space.index}
      {props.space['has-focus'] || isHover ? apps : ''}
    </div>
  )
}
