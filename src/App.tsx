import React, { useEffect } from 'react'
import Spaces from './components/spaces'
import { YabaiSpacesProvider } from './context/socket/yabai-provider'
import { Time } from './components/time'
import { NetworkDataProvider } from './context/socket/network-provider'
import { Network } from './components/network'
import { CpuDataProvider } from './context/socket/cpu-provider'
import { Cpu } from './components/cpu'
import { MemoryDataProvider } from './context/socket/memory-provider'
import { Memory } from './components/memory'
import { DateCalendar } from './components/date'
import { BatteryDataProvider } from './context/socket/battery-provider'
import { Battery } from './components/battery'
import { KeyboardDataProvider } from './context/socket/keyboard-provider'
import { Keyboard } from './components/keyboard'

function App() {
  useEffect(() => {
    document.title = 'Acidspike Bar'
  }, [])

  return (
    <div className={'h-full flex'}>
      <div className={'flex w-1/2'}>
        <YabaiSpacesProvider>
          <Spaces></Spaces>
        </YabaiSpacesProvider>
      </div>
      <div className={'flex w-1/2 justify-end items-center p-1'}>
        <NetworkDataProvider>
          <Network />
        </NetworkDataProvider>
        <CpuDataProvider>
          <Cpu />
        </CpuDataProvider>
        <MemoryDataProvider>
          <Memory />
        </MemoryDataProvider>
        <BatteryDataProvider>
          <Battery />
        </BatteryDataProvider>
        <KeyboardDataProvider>
          <Keyboard />
        </KeyboardDataProvider>
        <DateCalendar />
        <Time />
      </div>
    </div>
  )
}

export default App
