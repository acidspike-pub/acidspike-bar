import React, { ReactNode, useEffect, useState } from 'react'
import { initSockets, registerListeners } from './socket-igniter'

export interface MemoryData {
  active: number
  total: number
}

export const MemoryDataContext = React.createContext<MemoryData>({
  total: 0,
  active: 0
})

interface Props {
  children: ReactNode
}

export const MemoryDataProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<MemoryData>({ active: 0, total: 0 })

  useEffect(() => {
    initMemoryDataSockets(setValue)
  }, [])

  return (
    <MemoryDataContext.Provider value={value}>
      {props.children}
    </MemoryDataContext.Provider>
  )
}

export const initMemoryDataSockets = (
  setValue: React.Dispatch<React.SetStateAction<MemoryData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerListeners('memory-data', data => {
    setValue(data)
  })
}
