import React, { useContext, useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'
import {
  BatteryData,
  BatteryDataContext
} from '../context/socket/battery-provider'
import { IconDefinition } from '@fortawesome/free-solid-svg-icons'

export const Battery: React.FC = () => {
  const batteryContext = useContext<BatteryData>(BatteryDataContext)
  const [iconValue, setIconValue] = useState<IconDefinition>(solid('battery'))

  useEffect(() => {
    if (batteryContext.isCharging) {
      setIconValue(solid('plug-circle-bolt'))
    } else if (batteryContext.acConnected) {
      setIconValue(solid('plug'))
    } else if (batteryContext.percent > 50 && batteryContext.percent <= 75) {
      setIconValue(solid('battery-three-quarters'))
    } else if (batteryContext.percent > 25 && batteryContext.percent <= 50) {
      setIconValue(solid('battery-half'))
    } else if (batteryContext.percent > 10 && batteryContext.percent <= 25) {
      setIconValue(solid('battery-quarter'))
    } else if (batteryContext.percent <= 10) {
      setIconValue(solid('battery-empty'))
    } else {
      setIconValue(solid('battery'))
    }
  }, [batteryContext])

  return (
    <div
      className={
        'flex mr-1 bg-emerald-400/50 text-white rounded-sm py-0.25 px-1 text-sm items-center h-full'
      }
    >
      <FontAwesomeIcon icon={iconValue} className={'mr-2'} />
      <div
        className={'w-1.5 bg-white self-end mb-0.5'}
        style={{ height: `calc(${batteryContext.percent}% - 0.25rem)` }}
      ></div>
    </div>
  )
}
