import React, { ReactNode, useEffect, useState } from 'react'
import { initSockets, registerListeners } from './socket-igniter'
import filesize from 'filesize'

export interface NetworkDataInterface {
  default: boolean
  iface: string
  ip4: string
  ip6: string
  mac: string
  type: string
}

export interface NetworkDataConnection {
  ssid: string
}

export interface NetworkDataStats {
  rx_sec: number
  tx_sec: number
  rx_sec_string: string
  tx_sec_string: string
}

export interface NetworkData {
  connection?: NetworkDataConnection
  currentInterface?: NetworkDataInterface
  stats?: NetworkDataStats
}

export const NetworkDataContext = React.createContext<NetworkData>({})

interface Props {
  children: ReactNode
}

export const NetworkDataProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<NetworkData>({})

  useEffect(() => {
    initNetworkDataSockets(setValue)
  }, [])

  return (
    <NetworkDataContext.Provider value={value}>
      {props.children}
    </NetworkDataContext.Provider>
  )
}

export const initNetworkDataSockets = (
  setValue: React.Dispatch<React.SetStateAction<NetworkData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerListeners('network-data', data => {
    data.stats.rx_sec_string = filesize(data.stats.rx_sec)
    data.stats.tx_sec_string = filesize(data.stats.tx_sec)
    setValue(data)
  })
}
