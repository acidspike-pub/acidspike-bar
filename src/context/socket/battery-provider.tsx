import React, { ReactNode, useEffect, useState } from 'react'
import { initSockets, registerListeners } from './socket-igniter'

export interface BatteryData {
  hasBattery: boolean
  isCharging: boolean
  percent: number
  timeRemaining: number
  acConnected: boolean
}

export const BatteryDataContext = React.createContext<BatteryData>({
  acConnected: false,
  hasBattery: false,
  isCharging: false,
  percent: 0,
  timeRemaining: 0
})

interface Props {
  children: ReactNode
}

export const BatteryDataProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<BatteryData>({
    acConnected: false,
    hasBattery: false,
    isCharging: false,
    percent: 0,
    timeRemaining: 0
  })

  useEffect(() => {
    initBatteryDataSockets(setValue)
  }, [])

  return (
    <BatteryDataContext.Provider value={value}>
      {props.children}
    </BatteryDataContext.Provider>
  )
}

export const initBatteryDataSockets = (
  setValue: React.Dispatch<React.SetStateAction<BatteryData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerListeners('battery-data', data => {
    setValue(data)
  })
}
