import { io, Socket } from 'socket.io-client'

let ioInstance: Socket | null = null
export const initSockets = () => {
  if (ioInstance) {
    return ioInstance
  }
  ioInstance = io('ws://localhost:16802', {
    reconnection: true
  })

  ioInstance.connect()

  ioInstance.on('connect', () => {
    console.log('Socket connected')
  })

  return ioInstance
}

export const registerListeners = (
  internalCommandName: string,
  handler: (...args: Array<any>) => void
) => {
  if (!ioInstance || !ioInstance.connected) {
    console.warn('No Socket IO instance, retrying in 5s')
    setTimeout(() => {
      registerListeners(internalCommandName, handler)
    }, 5000)
    return
  }

  ioInstance.off(internalCommandName)

  ioInstance.on(internalCommandName, handler)

  ioInstance.emit('registerListeners', {
    internalCommandName
  })
}

export const registerCommand = (
  commandName: string,
  command: string,
  args: Array<string>,
  interval: number,
  handler: (...args: Array<any>) => void
) => {
  if (!ioInstance || !ioInstance.connected) {
    console.warn('No Socket IO instance, retrying in 5s')
    setTimeout(() => {
      registerCommand(commandName, command, args, interval, handler)
    }, 5000)
    return
  }

  ioInstance.off(commandName)

  ioInstance.on(commandName, handler)

  ioInstance.emit('registerCommand', {
    commandName,
    command,
    args,
    interval
  })
}

export const unregisterCommand = (commandName: string) => {
  if (!ioInstance || !ioInstance.connected) {
    console.warn('No Socket IO instance, retrying in 5s')
    setTimeout(() => {
      unregisterCommand(commandName)
    }, 5000)
    return
  }

  ioInstance.off(commandName)

  ioInstance.emit('unregisterCommand', {
    commandName
  })
}

export const runCommand = (command: string, args: Array<string>) => {
  if (!ioInstance || !ioInstance.connected) {
    console.warn('No Socket IO instance, retrying in 5s')
    return
  }

  ioInstance.emit('runCommand', {
    command,
    args
  })
}
