import * as kl from 'keyboard-layout'
let interval: NodeJS.Timer = null
let running = false

export function handleKeyboardData(server) {
  if (interval) {
    return
  }

  interval = setInterval(() => {
    if (running) {
      return
    }

    running = true
    server.emit('keyboard-data', kl.getCurrentKeyboardLayout())
    running = false
  }, 1000)
}
