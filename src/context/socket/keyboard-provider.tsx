import React, { ReactNode, useEffect, useState } from 'react'
import { initSockets, registerListeners } from './socket-igniter'

export interface KeyboardData {
  keyboardLayout: string
}

export const KeyboardDataContext = React.createContext<KeyboardData>({
  keyboardLayout: ''
})

interface Props {
  children: ReactNode
}

export const KeyboardDataProvider: React.FC<Props> = (props: Props) => {
  const [value, setValue] = useState<KeyboardData>({
    keyboardLayout: ''
  })

  useEffect(() => {
    initKeyboardDataSockets(setValue)
  }, [])

  return (
    <KeyboardDataContext.Provider value={value}>
      {props.children}
    </KeyboardDataContext.Provider>
  )
}

export const initKeyboardDataSockets = (
  setValue: React.Dispatch<React.SetStateAction<KeyboardData>>
) => {
  if (!setValue) {
    return
  }
  initSockets()

  registerListeners('keyboard-data', data => {
    setValue({ keyboardLayout: data.split('.').pop() })
  })
}
