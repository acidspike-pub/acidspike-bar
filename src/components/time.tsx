import { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro'

export const Time: React.FC = props => {
  const [value, setValue] = useState<string>('00:00')

  useEffect(() => {
    const interval = setInterval(() => {
      const date = new Date()
      setValue(date.toLocaleTimeString())
    })

    return () => clearInterval(interval)
  })
  return (
    <div
      className={
        'flex text-sm px-1 py-0.25 rounded-sm items-center text-white bg-red-500/50'
      }
    >
      <FontAwesomeIcon icon={solid('clock')} className={'mr-1'} />
      {value}
    </div>
  )
}
